import { swrFetcher } from "@/helper/Fetcher";
import useSWR from 'swr'
import { useRouter } from "next/router";
export { getServerSideProps } from './dataFetch'
const DetailVideo = ({data: videoDetail}) => {
    const router = useRouter();
    const { slug } = router.query
    const {data, error} = useSWR(`/movie/${slug}?api_key=2059a9296fa208b923c55dce3788ced5`, swrFetcher,{fallbackData: videoDetail})
    if(!data && !error) return <>Loading...</>
    return data && <>Page Detail Video {data?.original_title}</>
}

export default DetailVideo;