const fs = require('fs')

const validEnv = ['development', 'staging', 'uat', 'production']
const targetFile = ['.env.development', '.env.production']
const environment = process.argv.slice(2)[0]

if (!validEnv.includes(environment)) {
  throw Error(`Invalid environment name ${environment}. Choose only ${validEnv.join(' | ')}`)
}

targetFile.forEach(envFile => {
  fs.copyFileSync(`env/.env.${environment}`, envFile)
})

console.log(`Environment ${environment} successfully created!`)