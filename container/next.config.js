/** @type {import('next').NextConfig} */
const NextFederationPlugin = require('@module-federation/nextjs-mf');
const nextConfig = {
  reactStrictMode: true,
  assetPrefix: 'http://localhost:3000',
  images:{
    domains: ["image.tmdb.org"]
  },
  distDir: 'build',
}

const remotes = isServer => {
  const location = isServer ? 'ssr' : 'chunks';
  return {
    core: `core@${process.env.URL_CORE}/_next/static/${location}/remoteEntry.js`,
    video: `video@${process.env.URL_VIDEO}/_next/static/${location}/remoteEntry.js`
  };
};
module.exports = {
  ...nextConfig,
  webpack(config, options) {
    
    config.plugins.push(
      new NextFederationPlugin({
        name: 'container',
        filename: 'static/chunks/remoteEntry.js',
        exposes: {
        },
        remotes: remotes(options.isServer),
        shared: {},
        extraOptions:{
          automaticAsyncBoundary: true
        }
      }),
    );
    
    return config;
  }
}
