import VideoDetailRemote from 'video/vdp'
import ErrorBoundary from '../../components/ErrorBoundary'

export { getServerSideProps } from 'video/dataFetchVideo'
const DetailVideo = ({data}) => {
    
    return (
        <ErrorBoundary>
            <VideoDetailRemote data={data}/>
        </ErrorBoundary>
    )
}

export default DetailVideo