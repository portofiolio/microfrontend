import React from "react";
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
    console.error(error, errorInfo)
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <div style={{width:'auto',height:'50px', textAlign:'center',verticalAlign:'middle',fontSize:'14px',lineHeight:'50px',border:'2px solid red'}}>
        Something went wrong.
      </div>;
    }

    return this.props.children; 
  }
}

export default ErrorBoundary